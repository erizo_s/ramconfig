package by.ram.erizo.domen.repositories;

import java.util.List;

import by.ram.erizo.domen.entity.GoodsEntity;
import io.reactivex.Observable;

/**
 * Created by Erizo on 30.03.2018.
 */

public interface GoodsRepository {

    Observable<List<GoodsEntity>> getGoods(String offset);
}
