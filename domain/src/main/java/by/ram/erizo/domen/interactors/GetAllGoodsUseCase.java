package by.ram.erizo.domen.interactors;

import java.util.List;

import javax.inject.Inject;

import by.ram.erizo.domen.entity.GoodsEntity;
import by.ram.erizo.domen.executor.PostExecutionThread;
import by.ram.erizo.domen.repositories.GoodsRepository;
import io.reactivex.Observable;

/**
 * Created by Erizo on 30.03.2018.
 */

public class GetAllGoodsUseCase extends BaseUseCase {

    private GoodsRepository goodsRepository;

    @Inject
    public GetAllGoodsUseCase(PostExecutionThread postExecutionThread, GoodsRepository goodsRepository) {
        super(postExecutionThread);
        this.goodsRepository = goodsRepository;
    }

    public Observable<List<GoodsEntity>> getGoods(String offset) {
        return goodsRepository.getGoods(offset).subscribeOn(threadExecution).observeOn(postExecutionThread);
    }
}
