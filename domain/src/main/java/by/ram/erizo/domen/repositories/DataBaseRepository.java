package by.ram.erizo.domen.repositories;

import java.util.List;

import by.ram.erizo.domen.entity.BucketEntity;
import by.ram.erizo.domen.entity.DescriptionEntity;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;

/**
 * Created by Erizo on 11.04.2018.
 */

public interface DataBaseRepository {

    Completable save(DescriptionEntity descriptionEntity);

    Flowable<List<BucketEntity>> getDataFromBucket();
}
