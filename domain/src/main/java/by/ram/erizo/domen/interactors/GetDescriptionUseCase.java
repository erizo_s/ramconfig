package by.ram.erizo.domen.interactors;

import java.util.List;

import javax.inject.Inject;

import by.ram.erizo.domen.entity.DescriptionEntity;
import by.ram.erizo.domen.executor.PostExecutionThread;
import by.ram.erizo.domen.repositories.DescriptionRepository;
import io.reactivex.Observable;

/**
 * Created by Erizo on 05.04.2018.
 */

public class GetDescriptionUseCase extends BaseUseCase {

    private DescriptionRepository descriptionRepository;

    @Inject
    public GetDescriptionUseCase(PostExecutionThread postExecutionThread, DescriptionRepository descriptionRepository) {
        super(postExecutionThread);
        this.descriptionRepository = descriptionRepository;
    }

    public Observable<List<DescriptionEntity>> getDescription(String sku) {
        return descriptionRepository.getDescription(sku).subscribeOn(threadExecution).observeOn(postExecutionThread);
    }
}
