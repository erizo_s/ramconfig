package by.ram.erizo.domen.interactors;

import java.util.List;

import javax.inject.Inject;

import by.ram.erizo.domen.entity.BucketEntity;
import by.ram.erizo.domen.entity.DescriptionEntity;
import by.ram.erizo.domen.executor.PostExecutionThread;
import by.ram.erizo.domen.repositories.DataBaseRepository;
import io.reactivex.Completable;
import io.reactivex.Flowable;
/**
 * Created by Erizo on 11.04.2018.
 */

public class SaveBucketUseCase extends BaseUseCase {

    private DataBaseRepository dataBaseRepository;

    @Inject
    public SaveBucketUseCase(PostExecutionThread postExecutionThread, DataBaseRepository dataBaseRepository) {
        super(postExecutionThread);
        this.dataBaseRepository = dataBaseRepository;
    }

    public Completable save(DescriptionEntity descriptionEntity) {
        return dataBaseRepository.save(descriptionEntity).subscribeOn(threadExecution).observeOn(postExecutionThread);
    }

    public Flowable<List<BucketEntity>> getDataFromBucket() {
        return dataBaseRepository.getDataFromBucket().subscribeOn(threadExecution).observeOn(postExecutionThread);
    }

}
