package by.ram.erizo.domen.repositories;

import java.util.List;

import by.ram.erizo.domen.entity.DescriptionEntity;
import io.reactivex.Observable;

/**
 * Created by Erizo on 05.04.2018.
 */

public interface DescriptionRepository {

    Observable<List<DescriptionEntity>> getDescription(String sku);


}
