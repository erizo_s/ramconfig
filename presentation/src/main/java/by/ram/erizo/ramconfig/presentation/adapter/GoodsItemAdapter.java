package by.ram.erizo.ramconfig.presentation.adapter;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import by.ram.erizo.domen.entity.GoodsEntity;
import by.ram.erizo.ramconfig.base.BaseAdapter;
import by.ram.erizo.ramconfig.base.BaseItemViewHolder;
import by.ram.erizo.ramconfig.presentation.pages.ItemsGoodsViewModel;

/**
 * Created by Erizo on 24.03.2018.
 */

public class GoodsItemAdapter extends BaseAdapter<GoodsEntity, ItemsGoodsViewModel> {

    @NonNull
    @Override
    public BaseItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return GoodsItemHolder.create(parent, new ItemsGoodsViewModel());
    }

}