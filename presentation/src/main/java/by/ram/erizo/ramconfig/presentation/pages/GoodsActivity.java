package by.ram.erizo.ramconfig.presentation.pages;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;

import by.ram.erizo.ramconfig.R;
import by.ram.erizo.ramconfig.base.BaseMvvmActivity;
import by.ram.erizo.ramconfig.databinding.GoodsBinding;

/**
 * Created by Erizo on 24.03.2018.
 */

public class GoodsActivity extends BaseMvvmActivity<GoodsBinding, GoodsViewModel> {

    @Override
    public int provideLayoutId() {
        return R.layout.goods;
    }

    @Override
    public GoodsViewModel provideViewModel() {
        return ViewModelProviders.of(this).get(GoodsViewModel.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerView.setHasFixedSize(true);
        binding.recyclerView.setAdapter(viewModel.getGoodsAdapter());
        String s = getIntent().getExtras().getString("category");
        binding.getViewModel().getCategoryFromActivity(s);
        System.out.println(s);
    }

}