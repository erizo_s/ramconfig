package by.ram.erizo.ramconfig.presentation.adapter;

import android.support.annotation.NonNull;
import android.view.ViewGroup;

import by.ram.erizo.domen.entity.BucketEntity;
import by.ram.erizo.domen.entity.GoodsEntity;
import by.ram.erizo.ramconfig.base.BaseAdapter;
import by.ram.erizo.ramconfig.base.BaseItemViewHolder;
import by.ram.erizo.ramconfig.presentation.bucket.ItemsBucketViewModel;
import by.ram.erizo.ramconfig.presentation.pages.ItemsGoodsViewModel;

/**
 * Created by Erizo on 13.04.2018.
 */

public class BucketItemsAdapter  extends BaseAdapter<BucketEntity, ItemsBucketViewModel> {

    @NonNull
    @Override
    public BaseItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return BucketItemHolder.create(parent, new ItemsBucketViewModel());
    }

}
