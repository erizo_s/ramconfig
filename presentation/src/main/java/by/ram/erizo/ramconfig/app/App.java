package by.ram.erizo.ramconfig.app;

import android.app.Application;

import by.ram.erizo.ramconfig.injection.AppComponent;
import by.ram.erizo.ramconfig.injection.AppModule;
import by.ram.erizo.ramconfig.injection.DaggerAppComponent;

/**
 * Created by Erizo on 24.03.2018.
 */

public class App extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}