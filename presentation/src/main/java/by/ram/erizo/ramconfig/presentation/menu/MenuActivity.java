package by.ram.erizo.ramconfig.presentation.menu;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import by.ram.erizo.ramconfig.R;
import by.ram.erizo.ramconfig.base.BaseMvvmActivity;
import by.ram.erizo.ramconfig.databinding.ActivityMainBinding;
import by.ram.erizo.ramconfig.presentation.bucket.BucketActivity;
import by.ram.erizo.ramconfig.presentation.pages.GoodsActivity;

public class MenuActivity extends BaseMvvmActivity<ActivityMainBinding, MenuViewModel> {

    private static final String EXTRA_ID = "category";

    @Override
    public int provideLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public MenuViewModel provideViewModel() {
        return ViewModelProviders.of(this).get(MenuViewModel.class);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding.bucketFromMenu.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), BucketActivity.class);
            startActivity(intent);
        });
        binding.motherboardImageButton.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), GoodsActivity.class);
            intent.putExtra("category", "9");
            startActivity(intent);
        });
        binding.cpuImageButton.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), GoodsActivity.class);
            intent.putExtra("category", "8");
            startActivity(intent);
        });
        binding.hddImageButton.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), GoodsActivity.class);
            intent.putExtra("category", "90");
            startActivity(intent);
        });
        binding.ramImageButton.setOnClickListener(view -> {
            Intent intent = new Intent(getApplicationContext(), GoodsActivity.class);
            intent.putExtra("category", "17");
            startActivity(intent);
        });
    }
}
