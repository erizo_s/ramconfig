package by.ram.erizo.ramconfig.base;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.Nullable;

import io.reactivex.disposables.CompositeDisposable;

/**
 * Created by Erizo on 12.03.2018.
 */

public abstract class BaseViewModel extends ViewModel {
    //нужно создавать здесь что бы не засорять UserViewModel
    //забрасываем в него
    //все подписки и можно отписать сразу отовсех
    //compositeDisposable.add(забрасываем диспособлы)
    protected CompositeDisposable compositeDisposable = new CompositeDisposable();

    public abstract void createInject();

    public BaseViewModel() {
        createInject();
    }


    public void onResume() {

    }

    public void onPause() {

    }

    public void onStart() {

    }

    public void onStop() {

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}