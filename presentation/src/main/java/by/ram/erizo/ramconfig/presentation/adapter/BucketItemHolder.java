package by.ram.erizo.ramconfig.presentation.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import by.ram.erizo.domen.entity.BucketEntity;
import by.ram.erizo.ramconfig.base.BaseItemViewHolder;
import by.ram.erizo.ramconfig.databinding.BucketItemBinding;
import by.ram.erizo.ramconfig.presentation.bucket.ItemsBucketViewModel;

/**
 * Created by Erizo on 13.04.2018.
 */

public class BucketItemHolder extends BaseItemViewHolder<BucketEntity,
        ItemsBucketViewModel, BucketItemBinding> {


    public BucketItemHolder(BucketItemBinding binding, ItemsBucketViewModel viewModel) {
        super(binding, viewModel);
    }

    public static BucketItemHolder create(ViewGroup parent, ItemsBucketViewModel viewModel) {
        BucketItemBinding binding = BucketItemBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new BucketItemHolder(binding, new ItemsBucketViewModel());
    }

}
