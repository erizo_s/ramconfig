package by.ram.erizo.ramconfig.presentation.bucket;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import org.reactivestreams.Subscription;

import java.util.List;

import javax.inject.Inject;

import by.ram.erizo.domen.entity.BucketEntity;
import by.ram.erizo.domen.interactors.SaveBucketUseCase;
import by.ram.erizo.ramconfig.app.App;
import by.ram.erizo.ramconfig.base.BaseViewModel;
import by.ram.erizo.ramconfig.presentation.adapter.BucketItemsAdapter;
import io.reactivex.FlowableSubscriber;

/**
 * Created by Erizo on 13.04.2018.
 */

public class BucketViewModel extends BaseViewModel {

    @SuppressLint("StaticFieldLeak")
    private AppCompatActivity activity;

    @Inject
    public SaveBucketUseCase saveBucketUseCase;

    @SuppressLint("StaticFieldLeak")
    @Inject
    public Context context;

    public BucketViewModel(AppCompatActivity activity) {
        this.activity = activity;
    }


    @Override
    public void createInject() {
        App.getAppComponent().inject(this);
    }

    private BucketItemsAdapter adapter = new BucketItemsAdapter();

    public BucketItemsAdapter getBucketAdapter() {
        return adapter;
    }

    public BucketViewModel() {
        saveBucketUseCase.getDataFromBucket().subscribe(new FlowableSubscriber<List<BucketEntity>>() {
            @Override
            public void onSubscribe(Subscription s) {

            }

            @Override
            public void onNext(List<BucketEntity> bucketEntities) {
                adapter.setItems(bucketEntities);
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

}
