package by.ram.erizo.ramconfig.presentation.pages;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;

import java.util.List;

import javax.inject.Inject;

import by.ram.erizo.domen.entity.GoodsEntity;
import by.ram.erizo.domen.interactors.GetAllGoodsUseCase;
import by.ram.erizo.ramconfig.app.App;
import by.ram.erizo.ramconfig.base.BaseAdapter;
import by.ram.erizo.ramconfig.base.BaseViewModel;
import by.ram.erizo.ramconfig.presentation.adapter.GoodsItemAdapter;
import by.ram.erizo.ramconfig.presentation.description.DescriptionActivity;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Erizo on 24.03.2018.
 */

public class GoodsViewModel extends BaseViewModel {

    @SuppressLint("StaticFieldLeak")
    @Inject
    public Context context;

    @Inject
    public GetAllGoodsUseCase getAllGoodsUseCase;

    private GoodsItemAdapter adapter = new GoodsItemAdapter();

    public GoodsItemAdapter getGoodsAdapter() {
        return adapter;
    }

    public GoodsViewModel() {
        adapter.observClick().subscribe(new Observer<BaseAdapter.ItemEntity>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(BaseAdapter.ItemEntity itemEntity) {
                Intent intent = new Intent(context, DescriptionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                GoodsEntity goodsEntity = (GoodsEntity) itemEntity.model;
                intent.putExtra("sku", goodsEntity.getSku());
                context.startActivity(intent);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void createInject() {
        App.getAppComponent().inject(this);
    }


    private void getGoods(String offset) { // можно унаследоваться эт этого класса , чтобы код был чище
        getAllGoodsUseCase.getGoods(offset).subscribe(new Observer<List<GoodsEntity>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(List<GoodsEntity> goodsEntities) {
                adapter.setItems(goodsEntities);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        });
    }

    public void getCategoryFromActivity(String s) {
        getGoods(s);
    }
}
