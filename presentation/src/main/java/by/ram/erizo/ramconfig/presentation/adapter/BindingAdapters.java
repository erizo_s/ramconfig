package by.ram.erizo.ramconfig.presentation.adapter;

import android.databinding.BindingAdapter;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

/**
 * Created by Erizo on 08.04.2018.
 */

public class BindingAdapters {

    @BindingAdapter("src")
    public static void loadImage(ImageView view, String url) {
        if (url == null)
            return;
        Log.d("TAG", url);
        Glide.with(view.getContext()).load(url).into(view);
    }

}
