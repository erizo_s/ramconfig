package by.ram.erizo.ramconfig.presentation.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import by.ram.erizo.domen.entity.GoodsEntity;
import by.ram.erizo.ramconfig.base.BaseItemViewHolder;
import by.ram.erizo.ramconfig.databinding.GoodsItemBinding;
import by.ram.erizo.ramconfig.presentation.pages.ItemsGoodsViewModel;

/**
 * Created by Erizo on 02.04.2018.
 */

public class GoodsItemHolder extends BaseItemViewHolder<GoodsEntity,
        ItemsGoodsViewModel, GoodsItemBinding> {

    public GoodsItemHolder(GoodsItemBinding binding, ItemsGoodsViewModel viewModel) {
        super(binding, viewModel);
    }

    public static GoodsItemHolder create(ViewGroup parent, ItemsGoodsViewModel viewModel) {
        GoodsItemBinding binding = GoodsItemBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new GoodsItemHolder(binding, new ItemsGoodsViewModel());
    }

}
