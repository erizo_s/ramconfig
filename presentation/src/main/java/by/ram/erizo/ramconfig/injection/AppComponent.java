package by.ram.erizo.ramconfig.injection;

import javax.inject.Singleton;

import by.ram.erizo.ramconfig.presentation.bucket.BucketViewModel;
import by.ram.erizo.ramconfig.presentation.description.DescriptionViewModel;
import by.ram.erizo.ramconfig.presentation.menu.MenuViewModel;
import by.ram.erizo.ramconfig.presentation.pages.GoodsViewModel;
import dagger.Component;

@Component(modules = {AppModule.class})
@Singleton
public interface AppComponent {

    void inject(GoodsViewModel goodsViewModel);

    void inject(MenuViewModel menuViewModel);

    void inject(DescriptionViewModel descriptionViewModel);

    void inject(BucketViewModel bucketViewModel);

}
