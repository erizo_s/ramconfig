package by.ram.erizo.ramconfig.presentation.pages;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;

import by.ram.erizo.data.entity.GoodsModel;
import by.ram.erizo.domen.entity.GoodsEntity;
import by.ram.erizo.ramconfig.base.BaseItemViewModel;

/**
 * Created by Erizo on 24.03.2018.
 */

public class ItemsGoodsViewModel extends BaseItemViewModel<GoodsEntity> {

    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> sku = new ObservableField<>();
    public ObservableField<String> price = new ObservableField<>();

    @Override
    public void setItem(GoodsEntity goodsEntity, int position) {
        name.set(goodsEntity.getText());
        sku.set("Номер товара: " + goodsEntity.getSku());
        price.set(goodsEntity.getPrice() + " " + "руб.");
    }
}
