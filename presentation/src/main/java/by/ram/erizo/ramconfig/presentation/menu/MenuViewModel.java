package by.ram.erizo.ramconfig.presentation.menu;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.databinding.InverseMethod;
import android.support.v7.app.AppCompatActivity;

import org.reactivestreams.Subscription;

import javax.inject.Inject;

import by.ram.erizo.domen.entity.DescriptionEntity;
import by.ram.erizo.domen.interactors.SaveBucketUseCase;
import by.ram.erizo.ramconfig.app.App;
import by.ram.erizo.ramconfig.base.BaseViewModel;
import by.ram.erizo.ramconfig.presentation.pages.GoodsActivity;
import io.reactivex.Flowable;
import io.reactivex.FlowableSubscriber;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Erizo on 19.03.2018.
 */

public class MenuViewModel extends BaseViewModel {

    @SuppressLint("StaticFieldLeak")
    private AppCompatActivity activity;

    public MenuViewModel(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void createInject() {
        App.getAppComponent().inject(this);
    }

    public MenuViewModel() {

    }

}
