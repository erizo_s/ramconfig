package by.ram.erizo.ramconfig.presentation.bucket;

import android.databinding.ObservableField;

import by.ram.erizo.domen.entity.BucketEntity;
import by.ram.erizo.ramconfig.base.BaseItemViewModel;

/**
 * Created by Erizo on 13.04.2018.
 */

public class ItemsBucketViewModel extends BaseItemViewModel<BucketEntity> {

    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> sku = new ObservableField<>();
    public ObservableField<String> price = new ObservableField<>();

    @Override
    public void setItem(BucketEntity bucketEntity, int position) {
        name.set(bucketEntity.getText());
        sku.set("Номер товара: " + bucketEntity.getSku());
        price.set(bucketEntity.getPrice() + " " + "руб.");
    }
}
