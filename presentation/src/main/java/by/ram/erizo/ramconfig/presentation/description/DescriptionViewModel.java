package by.ram.erizo.ramconfig.presentation.description;

import android.annotation.SuppressLint;
import android.content.Context;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import by.ram.erizo.domen.entity.DescriptionEntity;
import by.ram.erizo.domen.interactors.GetDescriptionUseCase;
import by.ram.erizo.domen.interactors.SaveBucketUseCase;
import by.ram.erizo.ramconfig.app.App;
import by.ram.erizo.ramconfig.base.BaseViewModel;
import io.reactivex.CompletableObserver;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Erizo on 05.04.2018.
 */

public class DescriptionViewModel extends BaseViewModel {

    private static final String TAG = DescriptionViewModel.class.getSimpleName();

    public ObservableField<String> name = new ObservableField<>();
    public ObservableField<String> price = new ObservableField<>();
    public ObservableField<String> image = new ObservableField<>();
    public ObservableField<String> shortDescription = new ObservableField<>();
    public ObservableField<String> skuGoods = new ObservableField<>();
    public ObservableBoolean loaded = new ObservableBoolean(false);
    public String skuFromActivity;


    @SuppressLint("StaticFieldLeak")
    private AppCompatActivity activity;

    @Inject
    public GetDescriptionUseCase getDescriptionUseCase;

    @Inject
    public SaveBucketUseCase saveBucketUseCase;

    @SuppressLint("StaticFieldLeak")
    @Inject
    public Context context;

    public DescriptionViewModel(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void createInject() {
        App.getAppComponent().inject(this);
    }

    public DescriptionViewModel() {
    }

    private void getDescription(String sku) {
        loaded.set(true);
        getDescriptionUseCase.getDescription(sku).subscribe(new Observer<List<DescriptionEntity>>() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onNext(List<DescriptionEntity> descriptionEntities) {
                name.set(descriptionEntities.get(0).getName());
                price.set(descriptionEntities.get(0).getPrice() + " " + "руб.");
                image.set(descriptionEntities.get(0).getUrlImages());
                skuGoods.set(descriptionEntities.get(0).getSku());
                shortDescription.set(descriptionEntities.get(0).getShortDescription());
            }

            @Override
            public void onError(Throwable e) {
                loaded.set(false);
            }

            @Override
            public void onComplete() {
                loaded.set(false);
            }
        });

    }

    public void getSkuFromActivity(String sku) {
        getDescription(sku);
        loaded.set(true);
    }

    public ObservableBoolean getLoaded() {
        return loaded;
    }

    public void setLoaded(ObservableBoolean loaded) {
        this.loaded = loaded;
    }

    public void saveItemToDB() {
        DescriptionEntity descriptionEntity = new DescriptionEntity();
        descriptionEntity.setName(name.get());
        descriptionEntity.setPrice(price.get());
        descriptionEntity.setSku(skuGoods.get());
        saveBucketUseCase.save(descriptionEntity).subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
                compositeDisposable.add(d);
            }

            @Override
            public void onComplete() {
                Toast.makeText(context, "Товар добавлен в корзину", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }
}
