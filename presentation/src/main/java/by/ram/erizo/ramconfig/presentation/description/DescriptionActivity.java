package by.ram.erizo.ramconfig.presentation.description;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;

import by.ram.erizo.ramconfig.R;
import by.ram.erizo.ramconfig.base.BaseMvvmActivity;
import by.ram.erizo.ramconfig.databinding.DescriptionActivityBinding;

/**
 * Created by Erizo on 05.04.2018.
 */

public class DescriptionActivity extends BaseMvvmActivity<DescriptionActivityBinding, DescriptionViewModel > {

    boolean isImageFitToScreen;

    @Override
    public int provideLayoutId() {
        return R.layout.description_activity;
    }

    @Override
    public DescriptionViewModel provideViewModel() {
        return ViewModelProviders.of(this).get(DescriptionViewModel.class);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String s = getIntent().getExtras().getString("sku");
        binding.getViewModel().getSkuFromActivity(s);
    }


}
