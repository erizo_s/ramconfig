package by.ram.erizo.ramconfig.presentation.bucket;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;

import by.ram.erizo.ramconfig.R;
import by.ram.erizo.ramconfig.base.BaseMvvmActivity;
import by.ram.erizo.ramconfig.base.BaseViewModel;
import by.ram.erizo.ramconfig.databinding.BucketActivityBinding;

/**
 * Created by Erizo on 13.04.2018.
 */

public class BucketActivity extends BaseMvvmActivity<BucketActivityBinding, BucketViewModel> {

    @Override
    public int provideLayoutId() {
        return R.layout.bucket_activity;
    }

    @Override
    public BucketViewModel provideViewModel() {
        return ViewModelProviders.of(this).get(BucketViewModel.class);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding.recyclerViewBucket.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerViewBucket.setHasFixedSize(true);
        binding.recyclerViewBucket.setAdapter(viewModel.getBucketAdapter());
    }
}
