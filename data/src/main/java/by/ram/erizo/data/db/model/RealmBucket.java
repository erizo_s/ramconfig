package by.ram.erizo.data.db.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Erizo on 09.04.2018.
 */

public class RealmBucket extends RealmObject {

    private String name;
    private String sku;
    private String price;

    public RealmBucket() {
    }

    public RealmBucket(String name, String sku, String price) {
        this.name = name;
        this.sku = sku;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
