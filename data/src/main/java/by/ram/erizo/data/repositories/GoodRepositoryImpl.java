package by.ram.erizo.data.repositories;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import by.ram.erizo.data.entity.GoodsModel;
import by.ram.erizo.data.net.RestService;
import by.ram.erizo.domen.entity.GoodsEntity;
import by.ram.erizo.domen.repositories.GoodsRepository;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by Erizo on 30.03.2018.
 */

public class GoodRepositoryImpl implements GoodsRepository {

    private Context context;
    private RestService restService;

    @Inject
    public GoodRepositoryImpl(Context context, RestService restService) {
        this.context = context;
        this.restService = restService;
    }

    @Override
    public Observable<List<GoodsEntity>> getGoods(String offset) {
        return restService.loadGoods(offset).map(goodsModels ->{
            List<GoodsEntity> goodsEntities = new ArrayList<>();
            for (GoodsModel item: goodsModels) {
                goodsEntities.add(new GoodsEntity(item.getName(), item.getSku(), item.getPrice()));
            }
            return goodsEntities;
        });
    }


}
