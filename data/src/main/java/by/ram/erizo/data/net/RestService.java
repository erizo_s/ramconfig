package by.ram.erizo.data.net;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import by.ram.erizo.data.entity.DescriptionModel;
import by.ram.erizo.data.entity.GoodsModel;
import by.ram.erizo.domen.entity.DescriptionEntity;
import io.reactivex.Observable;

/**
 * Created by Erizo on 30.03.2018.
 */
@Singleton
public class RestService {

    private RestApi restApi;

    @Inject
    public RestService(RestApi restApi) {
        this.restApi = restApi;
    }

    public Observable<List<GoodsModel>> loadGoods(String offset) {
        return restApi.loadGoods(offset);
    }

    public Observable<List<DescriptionModel>> getDescription(String sku) {
        return restApi.getDescription(sku);
    }

}
