package by.ram.erizo.data.net;

import java.util.List;

import by.ram.erizo.data.entity.DescriptionModel;
import by.ram.erizo.data.entity.GoodsModel;
import by.ram.erizo.domen.entity.DescriptionEntity;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Erizo on 30.03.2018.
 */

public interface RestApi {

    @GET("data_from_category.php?key=fbhsdkjerGHEW2Gvkd")
    Observable<List<GoodsModel>> loadGoods(@Query("category") String offset);

    @GET("data_from_sku.php?key=fbhsdkjerGHEW2Gvkd")
    Observable<List<DescriptionModel>> getDescription(@Query("sku") String sku);

}
