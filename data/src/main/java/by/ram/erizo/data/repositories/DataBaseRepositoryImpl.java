package by.ram.erizo.data.repositories;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import by.ram.erizo.data.db.model.RealmBucket;
import by.ram.erizo.domen.entity.BucketEntity;
import by.ram.erizo.domen.entity.DescriptionEntity;
import by.ram.erizo.domen.repositories.DataBaseRepository;
import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Erizo on 11.04.2018.
 */

public class DataBaseRepositoryImpl implements DataBaseRepository {

    private Context context;


    public Realm getRealm() {
        Realm.init(context.getApplicationContext());
        return Realm.getDefaultInstance();
    }

    @Inject
    public DataBaseRepositoryImpl(Context context) {
        this.context = context;
    }

    @Override
    public Completable save(DescriptionEntity descriptionEntity) {
        Realm realm = getRealm();
        realm.beginTransaction();
        RealmBucket bucket = realm.createObject(RealmBucket.class);
        bucket.setName(descriptionEntity.getName());
        bucket.setSku(descriptionEntity.getSku());
        bucket.setPrice(descriptionEntity.getPrice());
        realm.commitTransaction();
        return Completable.complete();
    }

    @Override
    public Flowable<List<BucketEntity>> getDataFromBucket() {
        List<BucketEntity> bucketEntityList = new ArrayList<>();
        Realm realm = getRealm();
        RealmResults<RealmBucket> realmBuckets = realm.where(RealmBucket.class).findAll();
        return Flowable.just(bucketEntityList);
    }
}
